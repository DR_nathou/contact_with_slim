<?php

/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 06/03/2017
 * Time: 16:22
 */
namespace App\Controllers;

class Controller
{

    protected $container;
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function render($response, $file, $array)
    {
        $this->container->view->render($response, $file, $array);
    }



}