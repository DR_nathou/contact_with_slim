<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 14/03/2017
 * Time: 20:01
 */

namespace App\Controllers;

class Login_Manager
{
    static function _existingUser($login)
    {
        try
        {
            if(self::_pseudoExist($login)) return "utilisateur non enregistré!";
            if(self::_pseudoExist($login) && self::_pwdIsValid($login)) return "mot de passe incorect!";

            $login->id_login = self::_selectIDlogin($login)[0]->id_login;
        }

        catch(\PDOException $e)
        {
            echo $e . "vous n'existez pas";
        }
        return false;
    }


    static function  _newUser($login)
    {
        try
        {
            if(!self::_pseudoExist($login))
            {
                $login->container['pdo']->prepare("INSERT INTO login(pseudo, mot_de_passe) VALUES (?,?)");
                $login->container['pdo']->execute([$login->pseudo, $login->mot_de_passe]);

                $login->id_login = self::_selectIDlogin($login)[0]->id_login;
            }
            else return "pseudo déjà utilisé!";
        }
        catch(\PDOException $e)
        {
            echo $e;
        }
        return false;
    }

    static function _pseudoExist($login)
    {
        try
        {
            $allLogin = self::_selectLogin($login->container['pdo']);
            for ($index = 0; $index < count($allLogin); $index++)
            {
                if ($login == $allLogin[$index]->pseudo)
                {
                    return true;
                }
            }
        }
        catch(\PDOException $e)
        {
            echo $e;
        }
        return false;
    }
    static function _pwdIsValid($login)
    {
        try
        {
            if(self::_selectLogin($login->container['pdo'])[0]->mot_de_passe !== $login->motDePasse) return false;

        }
        catch(\PDOException $e)
        {
            echo $e;
        }
        return true;
    }

    static function _SelectIDLogin($login)
    {
        $result = $login->container['pdo']->prepare("SELECT id_login FROM login WHERE pseudo =?")->EXECUTE([$login->pseudo]);
        return $result->fetchall(\PDO::FETCH_OBJ);
    }

    static function _selectLogin($pdo)
    {
        $result = $pdo->query("SELECT * FROM login");
        return $result->fetchAll(\PDO::FETCH_OBJ);
    }


}