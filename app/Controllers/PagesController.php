<?php

namespace App\Controllers;
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 06/03/2017
 * Time: 14:18
 *
 */

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Slim\App;
use  Slim\Views\Twig;

class PagesController extends Controller
{
    private function f5()
    {
        return Contact_Manager::_get_contact($this->container);
    }

    private function buildContact($new = false)
    {
        try
        {
            if($new)
            return new \App\DD\Contact($_POST['nom'], $_POST['prenom'], $_POST['email'], $_POST['adresse'], $_POST['telephone'], 0,"unknown", $this->container);
            else return new \App\DD\Contact($_POST['nom'], $_POST['prenom'], $_POST['email'], $_POST['adresse'], $_POST['telephone'], 0, $_POST['idcontact'], $this->container);

        }
        catch (\InvalidArgumentException $e)
        {
            echo "<script type='text/javascript'>alert('failed!')</script>";
        }
    }

    public function contact($request, $response)
    {
        $this->render($response, 'pages/contact.twig',
            array('contactList' => self::f5()));
    }

    public function logout($request, $response)
    {
        $this->render($response, 'pages/login.twig',array());
    }

    public function delete($request, $response)
    {
        $error = Contact_Manager::_delete(self::buildContact());
        $this->render($response, 'pages/contact.twig',
            array('contactList' =>self::f5()));
        echo "<script type='text/javascript'>alert('".$error."')</script>";
    }

    public function updateButton($request, $response)
    {
        $this->render($response, 'pages/contact.twig',
            array('contact' => self::buildContact(), 'contactList' => self::f5()));
    }

    public function updateNew($request, $response)
    {
        if($_POST['submit'] == 'enregistrer')
        {

            $error = Contact_Manager::_update(self::buildContact());
        }
        else
        {
            echo "bloooooooooooooooooooooooooooooooo";
            $error = Contact_Manager::_new(self::buildContact(true));
        }

        $this->render($response, 'pages/contact.twig',
            array('contact' => self::buildContact(), 'contactList' => self::f5()));
        echo "<script type='text/javascript'>alert('".$error."')</script>";
    }

    private function buildLogin()
    {
        return new \App\DD\Login($_POST['pseudo'], $_POST['motdepasse'], $this->container);
    }

    public function login($request, $response)
    {
        if($_POST['submit'] == 'Connecter')
        {
            $error = Login_Manager::_existingUser(self::buildLogin());
        }
        else $error = Login_Manager::_newUser(self::buildLogin());

        $this->contact($request, $response);
        echo "<script type='text/javascript'>alert('".$error."')</script>";
    }

    public function home($request, $response)
    {
        $this->render($response, 'pages/login.twig',
            array());
    }

}