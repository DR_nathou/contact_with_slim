<?php

/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 08/03/2017
 * Time: 20:56
 */
namespace App\DD;

class Contact
{
    public $idcontact, $nom, $prenom, $email, $adresse, $telephone, $idlogin;
    public $container;

    function __construct( $nom, $prenom, $email, $adresse, $telephone, $idlogin, $idcontact, $container)
    {
        $this->container = $container;
        $this->idcontact = $idcontact;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->email = $email;
        $this->telephone = $telephone;
        $this->adresse = $adresse;
        $this->idlogin = $idlogin;
    }

}