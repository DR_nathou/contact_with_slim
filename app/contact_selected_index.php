<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 06/02/2017
 * Time: 02:02
 */
include_once 'Contact.php';
include_once 'Login.php';
session_start();

if(isset($_POST['index']))// si on arrive sur cette page par la bonne porte
{
    $_SESSION["contact_selected_index"] = $_POST['index'];
}//definition de l'index tel que spécifié dans $_POST['index']
header("Location: contact_view.php");
exit();
