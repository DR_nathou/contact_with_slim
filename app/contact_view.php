<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 04/02/2017
 * Time: 22:45
 */

include_once "DB.php";
session_start();
// view all contact
$db = new DB();
$db->statement = "SELECT * FROM contact WHERE id_login =".$_SESSION["login"]->id_login.$_SESSION["option"];
$db->_selectContact();
//Remplissage de la liste des contact a afficher + options si options spécifiés

if (!isset($_SESSION["login"]))
{
    //header("Location: index.php?error=Veuillez vous connecter<br><pre>pour une demo<br>pseudo = test/ mot de passe = test ˁ˚ᴥ˚ˀ</pre>");
    //exit();
}// en cas de demande de cette page sans avoir été loggué au préalable redirige vers la page init (index.php)
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="css/css.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
    <title>phpcssjsmysqlcontacts</title>
</head>
<body>
<?php
    // set du contact à afficher dans la zone de modification /creation/supression d'un contact
    if($_SESSION["contact_selected_index"] > count($_SESSION["contactList"])) $_SESSION["contact_selected_index"] = 0;
    // mise à 0 de l'index du contact selectionné SI l'index selectionné n'existe pas dans la liste des contact à afficher
    elseif(count($_SESSION["contactList"]) == 0) $ContactWorkingON = new Contact("","","","","",$_SESSION["login"]->id_login);
    // Si la liste des contact a afficher est vide; On set le contact a afficher à "";
    elseif(isset($_GET["nom"])) $ContactWorkingON = new Contact($_GET["nom"],$_GET["prenom"],$_GET["email"],$_GET["adresse"],$_GET["telephone"],$_SESSION["login"]->id_login);
    // set du contact à afficher avec les variables de la methode $_GET settés dans contact_logic qui sont un retour des données en cas de données invalide
    else $ContactWorkingON = $_SESSION["contactList"][$_SESSION["contact_selected_index"]];
    //  au sinon/par defaut set le contact selectionné par clic par le user

?>

<div id="options">
    <form method="post" action="contact_sort.php">
        <input type="submit" name="logOff" value="Se deconnecter!">
        <input type="submit" name="letter" value="Tous">
        <label for="field">Rechercher par</label>
        <input type="radio" name="field" value="nom" checked><label for="nom">nom</label>
        <input type="radio" name="field" value="prenom"><label for="prenom">prenom</label>
        <label for="firstMidLast">qui </label>
        <input type="radio" name="firstMidLast" value="first" checked><label for="first">commence</label>
        <input type="radio" name="firstMidLast" value="mid"><label for="mid">contient</label>
        <input type="radio" name="firstMidLast" value="last"><label for="first">termine par</label>
        <br>
        <br>
        <?php
            foreach(range('a', 'z') as $letter)
        {?>
            <input type="submit" name="letter" value="<?php echo $letter ?>">
        <?php }
        ?>


    </form>
    <div id="error">
        <?php echo "<br>".$_SESSION["error"];?>
    </div>
</div>
<!--affichage des options d'affichage des contact -> traité dans le php contact_sort.php-->

<div id="bookElement">
    <div id="details">
        <form method="post" action="contact_logic.php">
            <label for="nom">nom</label>
            <input type="text" name="nom" value="<?php echo $ContactWorkingON->nom;?>">
            <br>
            <label for="prenom">prenom</label>
            <input type="text" name="prenom" value="<?php echo $ContactWorkingON->prenom;?>">
            <br>
            <label for="email">email</label>
            <input type="text" name="email" value="<?php echo $ContactWorkingON->email;?>">
            <br>
            <label for="telephone">telephone</label>
            <input type="text" name="telephone" value="<?php echo $ContactWorkingON->telephone;?>">
            <br>
            <label for="adresse">adresse</label>
            <input type="text" name="adresse" value="<?php echo $ContactWorkingON->adresse;?>">
            <br>
            <input type="submit" name="submit" value="Modifier">
            <input type="submit" name="submit" value="Creer">
            <input type="submit" name="submit" value="Suprimer">
        </form>
    </div> <!--form d'affichage et de modification/création/supression de contact-->
    <div id="contactList">
        <table>

            <tr>
                <th>Nom</th>
                <th>Prenom</th>
                <th> ˁ˚ᴥ˚ˀ</th>
            </tr>
            <?php
            for($index = 0; $index < count($_SESSION["contactList"]); $index++)
            {?>
                <tr>
                    <td><?php echo $_SESSION["contactList"][$index]->nom;?></td>
                    <td><?php echo $_SESSION["contactList"][$index]->prenom;?></td>


                    <td><form method="post" action="contact_selected_index.php">
                            <input type="hidden" name="index" value="<?php echo $index?>">
                            <input type="submit" name="submit" value="details">
                        </form></td>
                </tr>
            <?php }
            ?>
        </table>
    </div>
    <!--affichage de la liste des contacts généré plus haut
    + selection du contact a afficher traité dans contact_selected_index.php-->
</div>
</body>
</html>