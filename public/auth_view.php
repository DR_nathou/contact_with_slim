<?php

session_start();
$_SESSION["contact_selected_index"] = 0;
//raz de l'index du contact a afficher

if(!isset($_SESSION["error"]))
{
    header("Location: index.php");
    exit;
}
//Si on a été redirigé vers cette page a partir d'une autre page, c'est qu'il y a une erreur de connection(ex: deconnection)
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="css/csslogin.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
    <title>Authentification</title>
</head>


<div id="error">
    <?php echo $_SESSION["error"];?>
</div>
<!--affichage du message-->

<body>
<div id="loginZone">
    <form method="post" action="authentification_logic.php">
        <label for="pseudo">Login</label>
        <input type="text" name="pseudo"><br>
        <label for="motDePasse">Mot de passe</label>
        <input type="password" name="motDePasse"><br>

        <input type="submit" name="submit" value="Connecter">
        <input type="submit" name="submit" value="Créer un acces">
    </form>
    <!--formulaire de login-->
</div>
</body>
</html>
