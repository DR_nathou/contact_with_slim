<?php
require '../vendor/autoload.php';


session_start();


$app = new \Slim\App(['settings' => ['displayErrorDetails' => true]]);



require('../app/container.php');

$app->get('/', \App\Controllers\PagesController::class . ':home')->setName('index');
$app->get('/contact', \App\Controllers\PagesController::class . ':contact')->setName('contact');
$app->get('/logout', \App\Controllers\PagesController::class . ':logout')->setName('logout');
$app->get('/test', \App\Controllers\PagesController::class . ':show');

$app->post('/login', \App\Controllers\PagesController::class . ':contact')->setName('login');
$app->post('/contact', \App\Controllers\PagesController::class . ':contact');
$app->post('/modifier', \App\Controllers\PagesController::class . ':updateButton')->setName('modifier');
$app->post('/delete', \App\Controllers\PagesController::class . ':delete')->setName('delete');
$app->post('/modNew', \App\Controllers\PagesController::class . ':updateNew')->setName('updateNew');


$app->run();

